import {Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import * as COUNSELORS_LIST_CONFIG from './counselors-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {Observable, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-counselors-table',
  templateUrl: './counselors-table.component.html',
  styles: []
})
export class CounselorsTableComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public readonly config = COUNSELORS_LIST_CONFIG;
  private fromYear: any;
  private fromPeriod: any;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
        if (fragment === 'reload') {
          this.table.fetch(true);
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'active'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              active: item.active
            };
          });
        }
      }
    }
    return items;
  }


  /**
   * Disable selected student counselors
   */
  async disableAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(x => {
        return x.active;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'StudentCounselors/disable' : null,
          modalTitle: 'Counselors.DisableAction.Title',
          description: 'Counselors.DisableAction.Description',
          errorMessage: 'Counselors.DisableAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDisableAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes disable action for selected student counselors
   */
  executeDisableAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.toYear == null || data.toPeriod == null || data.endDate === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            item.toYear = data.toYear;
            item.toPeriod = data.toPeriod;
            item.endDate = data.endDate;
            item.active = false;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model('StudentCounselors').save(item);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  /**
   * Deletes selected course classes
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items without students
      this.selectedItems = items;
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Counselors.DeleteAction.Title',
          description: 'Counselors.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes delete action for student counselors
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('StudentCounselors').remove(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }
}
