import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-courses-overview-form',
  templateUrl: './courses-overview-form.component.html'
})
export class CoursesOverviewFormComponent implements OnInit, OnChanges, OnDestroy {

  // courseId is optional, if model is empty
  @Input() courseId: any;
  @Input() model: any;
  @Input() showEdit = true;
  @Input() parentCourse: any;
  /* an optional input, used to hide the "more" anchor when the course tab is already being displayed
     e.g. when this *reusable* component is created by the courses dashboard module. */
  @Input() sourceTab?: string;

  private reloadSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.reloadSubscription = this._activatedRoute.fragment.subscribe(async fragment => {
      if (fragment && fragment === 'reload' && this.courseId) {
        // reload data
        this.model = await this._context.model('Courses')
          .where('id').equal(this.courseId)
          .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
          .getItem();
      }
    });
    if (!this.model) {
      if (this.courseId) {
        this.model = await this._context.model('Courses')
          .where('id').equal(this.courseId)
          .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
          .getItem();
      }
    }
    if (typeof this.sourceTab === 'undefined') {
      this.sourceTab = 'notCoursesTab';
    }
  }

  async ngOnChanges(changes: SimpleChanges) {
    // operate only if model input changes value
    if (changes && changes.model && changes.model.previousValue !== changes.model.currentValue) {
      // this is a reusable component
      // any other component can pass not properly formatted
      // replacedCourse or replacedByCourse attributes, so handle them at this point
      const replacedByCourse = this.model && this.model.replacedByCourse;
      // if replacedByCourse exists
      if (replacedByCourse && typeof replacedByCourse !== 'object') {
        // get course
        const replacedByCourseData = await this._context.model('Courses')
            .where('id').equal(replacedByCourse)
            .select('id', 'displayCode', 'name')
            .getItem();
        // override model field
        this.model.replacedByCourse = replacedByCourseData;
      }
      const replacedCourse = this.model && this.model.replacedCourse;
      // if replacedCourse exists
      if (replacedCourse && !Array.isArray(replacedCourse)) {
        // get course ids
        const replacedCoursesIds = replacedCourse.split(',').map(id => id.trim());
        // define promise chain
        const courses = replacedCoursesIds.map(async courseId => {
          return await this._context.model('Courses').where('id').equal(courseId).select('id', 'displayCode', 'name').getItem();
        });
        // execute promises and get courses
        const replacedCourseUpdated = await Promise.all(courses);
        // override model field
        this.model.replacedCourse = replacedCourseUpdated;
      }
      // handle also parent course
      const parentCourse = this.model && this.model.parentCourse;
      if (this.parentCourse == null && parentCourse && typeof parentCourse !== 'object') {
        // fetch parent course
        const parentCourseData = await this._context.model('Courses')
          .where('id').equal(parentCourse)
          .select('id', 'name', 'displayCode')
          .getItem();
        // and override model field
        this.model.parentCourse = parentCourseData;
        // and local variable
        this.parentCourse = parentCourseData;
      }
    }
  }

  ngOnDestroy(): void {
      if (this.reloadSubscription) {
        this.reloadSubscription.unsubscribe();
      }
  }
}
