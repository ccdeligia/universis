import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesHomeComponent } from './theses-home.component';

describe('ThesesHomeComponent', () => {
  let component: ThesesHomeComponent;
  let fixture: ComponentFixture<ThesesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesHomeComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
