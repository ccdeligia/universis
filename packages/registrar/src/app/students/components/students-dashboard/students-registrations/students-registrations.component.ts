import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {DIALOG_BUTTONS, LoadingService, ModalService} from '@universis/common';
import {Subscription} from 'rxjs';
import {ActivatedTableService, AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import * as STUDENTS_REGISTRATIONS_LIST_CONFIG from './students-registrations.conflig.list.json';
import * as COURSES_REGISTRATION_CONFIG from './students-registrations.config.courses.json';
import * as COURSES_REGISTRATION_SEARCH from './students-registrations.search.courses.json';
import { ActiveDepartmentService } from 'packages/registrar/src/app/registrar-shared/services/activeDepartmentService.service';
import { TableColumnConfiguration } from '@universis/ngx-tables';
@Component({
  selector: 'app-students-registrations',
  templateUrl: './students-registrations.component.html',
  styleUrls: ['./students-registrations.component.scss']
})
export class StudentsRegistrationsComponent implements OnInit, OnDestroy  {
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_REGISTRATIONS_LIST_CONFIG;
  public recordsTotal: any;
  public allowNewRegistration: boolean;
  private dataSubscription: Subscription;
  @ViewChild('model') model: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  private studentId: any;
  public currentConfig: any;
  public showTeachingHours: boolean = true;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _activeDepartmentService: ActiveDepartmentService) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();

    // get activeDepartment
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    // get instituteId
    const instituteId = activeDepartment && activeDepartment.organization;
    // get institute
    const institute = await this._context.model('Institutes')
      .where('id').equal(instituteId).select('id', 'instituteConfiguration').getItem();
    // get showTeachingHours flag
    this.showTeachingHours = institute && institute.instituteConfiguration && institute.instituteConfiguration.showTeachingHours;

    this._activatedTable.activeTable = this.model;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.model.query = this._context.model('Students/' + params.id + '/registrations')
        .asQueryable()
        .orderByDescending('registrationYear')
        .thenByDescending('registrationPeriod')
        .prepare();
      this._loadingService.hideLoading();
      this.model.config = AdvancedTableConfiguration.cast(STUDENTS_REGISTRATIONS_LIST_CONFIG);
      this.model.fetch();

      const studentStatus = await this._context.model('Students')
        .where('id').equal(params.id)
        .select('studentStatus/alternateName as status')
        .expand('studentStatus')
        .getItem();

      this.allowNewRegistration = studentStatus.status === 'active';
      this.currentConfig = "list";
    });

    // reload using the hidden fragment emitted by new-registration modal component
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.model.fetch();
      }
    });
  }

  async openDialog() {
    await this._modalService.showDialog(this._translate.instant('Students.Registration.NewRegistration'),
                                        this._translate.instant('Students.Registration.NotActiveMsg'),
                                        DIALOG_BUTTONS.Ok);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }


  onLoading(event: { target: AdvancedTableComponent }){
    if(event && event.target && event.target.config){
      if (this.showTeachingHours) {
        // Find the column with the name "courseClass/weekHours" and set its "hidden" property to "false"
        const configColumns = event.target.config.columns || [];
        const weekHoursColumn = configColumns.find((column: TableColumnConfiguration) => column.name === 'hours');
        if (weekHoursColumn) {
          weekHoursColumn.hidden = false;
        }
      }
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  changeConfig(config: any) {
    try {
      if (config !== this.currentConfig) {
        if (config === "list") {
          this.currentConfig = "list"
          this.model.query = this._context.model('Students/' + this.studentId + '/registrations')
            .asQueryable()
            .orderByDescending('registrationYear')
            .thenByDescending('registrationPeriod')
            .prepare();
          this.model.config = AdvancedTableConfiguration.cast(STUDENTS_REGISTRATIONS_LIST_CONFIG);
          this.model.ngOnInit();
        } else if (config === "courses") {
          this.currentConfig = "courses";
          this.model.query = this._context.model('StudentCourseClasses')
            .asQueryable()
            .where('student').equal(this.studentId)
            .expand('registration')
            .orderByDescending('registration/registrationYear')
            .thenByDescending('registration/registrationPeriod')
            .prepare();

          this.model.config = AdvancedTableConfiguration.cast(COURSES_REGISTRATION_CONFIG);
          this.model.ngOnInit();

          setTimeout(() => {
            this.searchConfiguration = COURSES_REGISTRATION_SEARCH;
            this.search.form = this.searchConfiguration;
            Object.assign(this.search.form, { student: this.studentId });
            this.search.ngOnInit();
          })
        }
      }
    } catch (err) {
      console.error(err);
    }
  }
}
