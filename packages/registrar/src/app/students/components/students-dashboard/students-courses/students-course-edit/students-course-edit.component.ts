import { Component, Input, AfterViewInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { StudentsCoursesPercentagesEditComponent } from '../students-courses-percentages-edit/students-courses-percentages-edit.component';

@Component({
  selector: 'app-students-course-edit',
  templateUrl: './students-course-edit.component.html'
})
export class StudentsCourseEditComponent implements AfterViewInit {

  @Input() execute: Observable<any>;
  @Input() items: Array<any>;
  @Input() formEditName: string;
  @Input() toastHeader: string;
  @Input() courseProperties: any;
  @Input() reloadParent: () => void;
  @Input() deleteCourseButton: boolean;
  @Input() complexCourses: Array<any>;

  @ViewChild('partsEdit') partsEdit: StudentsCoursesPercentagesEditComponent;

  constructor() { }

  ngAfterViewInit() {
    // hide the modal title of StudentsCoursesPercentagesEdit component
    if (this.partsEdit) { this.partsEdit['modalTitle'] = ''; }
  }

}
