import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ErrorService, LoadingService, ModalService } from '@universis/common';
import { AdminService, PermissionMask, Targets } from '@universis/ngx-admin';
import { AdvancedRowActionComponent, AdvancedTableComponent } from '@universis/ngx-tables';
import { Observable } from 'rxjs';

@Component({
  selector: 'universis-document-publisher',
  template: `
  <div></div>
  `
})
export class DocumentPublisherComponent implements AfterViewInit {

    @Input() table: AdvancedTableComponent;
    private selectedItems = [];
    @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
    public canRevertPublication: boolean;
    private errors: any[] = [];

    constructor(
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _adminService: AdminService,
              private _translateService: TranslateService) {
        //
    }
  ngAfterViewInit(): void {
    const canRevertPublicationPermission = {
      model: 'DocumentNumberSeriesItem',
      privilege: 'DocumentNumberSeriesItem/CancelPublication',
      target: Targets.All,
      mask: PermissionMask.Execute
    };
    // do not await this call
    this._adminService.hasPermission(canRevertPublicationPermission)
      .then((validationResult: boolean) => {
        this.canRevertPublication = validationResult;
      }).catch(err => {
        console.error(err);
        this.canRevertPublication = true;
      });
  }

  async getSelectedItems() {
    let items = [];
    let lastQuery: ClientDataQueryable = (this.table && this.table.query) || (this.table && this.table.lastQuery);
    if (lastQuery != null) {
      if (this.table.smartSelect) {
        // get items
        const selectArguments = ['id', 'published', 'datePublished', 'signed'];
        // query items
        const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
        if (this.table.unselected && this.table.unselected.length) {
          // remove items that have been unselected by the user
          items = queryItems.filter( item => {
            return this.table.unselected.findIndex( (x) => {
              return x.id === item.id;
            }) < 0;
          });
        } else {
          items = queryItems;
        }
      } else {
        // get selected items only
        items = this.table.selected.map( (item) => {
          return {
            id: item.id,
            signed: item.signed,
            published: item.published,
            datePublished: item.datePublished
          };
        });
      }
    }
    return items;
  }


    private executePublishActionOne(item: any, index: number) {
        const total = this.selectedItems.length;
        return new Promise((resolve, reject) => {
          const id = item.id;
          this._context.model('DocumentNumberSeriesItems').save(item).then(() => {
            setTimeout(() => {
                this.refreshAction.emit({
                  progress: Math.floor(((index + 1) / total) * 100)
                });
                return this.table.fetchOne({
                  id
                }).then(() => {
                  return resolve({
                    success: 1
                  });
                }).catch((err) => {
                  let lastError = err;
                  if (err.error) {
                    lastError = err.error;
                  }
                  console.error(lastError);
                  return resolve({
                    success: 1,
                    error: lastError
                  });
                });
            }, 500);
          }).catch(err => {
            let lastError = err;
            if (err.error) {
              lastError = err.error;
            }
            console.error(lastError);
            this.errors.push(lastError);
            return resolve({
              success: 0,
              error: lastError
            });
          });
        });
      }
    
      executePublishAction() {
        return new Observable((observer) => {
          this.refreshAction.emit({
            progress: 1
          });
          const result = {
            total: this.selectedItems.length,
            success: 0,
            errors: 0
          };
          // execute update for all items (transactional update)
          const datePublished = new Date();
          // map items
          const updated = this.selectedItems.map((item) => {
            return {
              id: item.id,
              published: true,
              datePublished: datePublished
            };
          });
          // execute promises in series within an async method
          this.errors = [];
          (async () => {
            for (let i = 0 ; i < updated.length ; i++) {
              const item = updated[i];
              const res: any = await this.executePublishActionOne(item, i);
              result.success += res.success;
              result.errors += 1 - res.success;
            }
          })().then(() => {
            // make animation a bit smoother
            setTimeout(() => {
              observer.next(result);
              if (this.errors.length > 0) {
                const component: AdvancedRowActionComponent = this._modalService.modalRef && this._modalService.modalRef.content;
                if (component) {
                  // get first error
                  const firstError = this.errors[0];
                  // show first error description
                  let message = this._translateService.instant(firstError.message);
                  if (this.errors.length > 1) {
                    message += ' ';
                    message += this._translateService.instant('MoreErrors', { value: this.errors.length - 1});
                  }
                  component.lastError = new Error(message);
                }
              }
            }, 500);
          }).catch((err) => {
            observer.error(err);
          });
        });
      }
      /**
       * Publishes the selected requests
       */
      async publishAction() {
        try {
          this._loadingService.showLoading();
          const items = await this.getSelectedItems();
          this.selectedItems = items.filter((item) => {
            return (!!item.published === false);
          });
          this._loadingService.hideLoading();
          this._modalService.openModalComponent(AdvancedRowActionComponent, {
            class: 'modal-lg',
            keyboard: false,
            ignoreBackdropClick: true,
            initialState: {
              items: this.selectedItems,
              modalTitle: 'Documents.PublishAction.Title',
              modalIcon: 'far fa-newspaper mt-0',
              description: 'Documents.PublishAction.Description',
              refresh: this.refreshAction,
              execute: this.executePublishAction()
            }
          });
        } catch (err) {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      }
    
      async cancelPublicationAction() {
        try {
          // show loading
          this._loadingService.showLoading();
          // get selected items
          const items = await this.getSelectedItems();
          // filter selected items
          this.selectedItems = items.filter((item) => {
            return !!item.published;
          });
          // hide loading
          this._loadingService.hideLoading();
          // open modal
          this._modalService.openModalComponent(AdvancedRowActionComponent, {
            class: 'modal-lg',
            keyboard: false,
            ignoreBackdropClick: true,
            initialState: {
              items: this.selectedItems,
              modalTitle: 'Documents.CancelPublishAction.Title',
              description: 'Documents.CancelPublishAction.Description',
              refresh: this.refreshAction,
              execute: this.executeCancelPublicationAction()
            }
          });
        } catch (err) {
          // hide loading
          this._loadingService.hideLoading();
          // show error
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      }
    
      executeCancelPublicationAction() {
        return new Observable((observer) => {
          this.refreshAction.emit({
            progress: 1
          });
          const total = this.selectedItems.length;
          const result = {
            total: total,
            success: 0,
            errors: 0
          };
          // execute promises in series within an async method
          (async () => {
            for (let i = 0; i < this.selectedItems.length; i++) {
              const item = this.selectedItems[i];
              const res: any = await this.executeCancelPublicationActionOne(item, i);
              result.success += res.success;
              result.errors += 1 - res.success;
            }
          })().then(() => {
            observer.next(result);
          }).catch((err) => {
            observer.error(err);
          });
        });
      }
    
      private executeCancelPublicationActionOne(item: any, index: number) {
        const total = this.selectedItems.length;
        return new Promise((resolve) => {
          this._context.model(`DocumentNumberSeriesItems/${item.id}/cancelPublication`).save(null).then(() => {
            setTimeout(() => {
                this.refreshAction.emit({
                  progress: Math.floor(((index + 1) / total) * 100)
                });
                return this.table.fetchOne({
                  id: item.id
                }).then(() => {
                  return resolve({
                    success: 1
                  });
                }).catch((err) => {
                  console.log(err);
                  return resolve({
                    success: 1,
                    error: err
                  });
                });
            }, 500);
          }).catch(err => {
              console.error(err);
              return resolve({
                success: 0,
                error: err
              });
          });
        });
      }

}