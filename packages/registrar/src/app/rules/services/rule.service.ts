import {Inject, Injectable} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {RULES_CONFIG, RuleConfiguration, DataModelRule} from './rule.configuration';
import {ClientDataModel} from '@themost/client';

@Injectable()
export class ItemRule {
  constructor() {
  }
  public id?: number;
  public target?: string;
  public targetType?: string;
  public refersTo?: string;
  public additionalType?: string;
  public checkValues?: string;
  public ruleOperator?: number;
  public value1?: string;
  public value2?: string;
  public value3?: string;
  public value4?: string;
  public value5?: string;
  public value6?: string;
  public value7?: string;
  public value8?: string;
  public value9?: string;
  public value10?: string;
  public value11?: string;
  public value12?: string;
  public value13?: string;
  public ruleExpression?: string;
  public programSpecialty?: number;
}

@Injectable()
export class RuleService {

  private configuration: RuleConfiguration;

  constructor(private _context: AngularDataContext, @Inject(RULES_CONFIG) config: RuleConfiguration) {
    this.configuration = config;
  }

  get(entitySet: string, object: any, navigationProperty: string): ClientDataModel {
    // encode string object (id)
    // note: do not encode numerical ids
    const targetObject = typeof object === 'string' ? encodeURIComponent(object) : object;
    return this._context.model(`${entitySet}/${targetObject}/${navigationProperty}`);
  }

  getConfiguration(entitySet: string, navigationProperty: string): DataModelRule {
    return this.configuration.types.find((item) => {
      return item.entitySet === entitySet && item.navigationProperty === navigationProperty;
    });
  }

}
