import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import {
  AdvancedTableModalBaseComponent,
  AdvancedTableModalBaseTemplate,
} from '@universis/ngx-tables';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';

@Component({
  selector: 'app-departments-add-place',
  template: AdvancedTableModalBaseTemplate,
})
export class DepartmentsAddPlacesComponent extends AdvancedTableModalBaseComponent {
  @Input() department: any;
  public placesToBeInserted = 0;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    protected _context: AngularDataContext,
    private _errorService: ErrorService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    protected advancedFilterValueProvider: AdvancedFilterValueProvider,
    protected datePipe: DatePipe
  ) {
    super(
      _router,
      _activatedRoute,
      _context,
      advancedFilterValueProvider,
      datePipe
    );
    // set default title
    this.modalTitle = 'Departments.AddPlace';
  }

  hasInputs(): Array<string> {
    return ['department'];
  }

  async ok(): Promise<any> {
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      items = selected.map(async (place) => {
        // get place id.
        const selectedPlaceId = place.id;
        // get selected place.
        const selectedPlace = await this._context
          .model('Places')
          .where('id')
          .equal(selectedPlaceId)
          .expand('departments')
          .getItem();
        // get place departments.
        const placeDepartments = selectedPlace.departments;
        // find department index.
        const departmentIndex = placeDepartments.findIndex(
          (someDepartment) => someDepartment.id === this.department.id
        );
        // if the department does not already exist, a new place is to be added.
        if (departmentIndex < 0) {
          this.placesToBeInserted++;
        }
        const department = { id: this.department.id };
        return {
          id: place.id,
          departments: [department]
        };
      });
      // execute promises and get places to be saved.
      const placesToBeSaved = await Promise.all(items);
      // if there are no new places to be inserted, inform and close.
      if (this.placesToBeInserted === 0) {
        this._toastService.show(
          this._translateService.instant('Departments.AddPlacesMessage.title'),
          this._translateService.instant(
            'Departments.AddPlacesMessage.AllAlreadyExist'
          )
        );
        return this.close();
      }
      // save places.
      return this._context
        .model('Places')
        .save(placesToBeSaved)
        .then(() => {
          this._toastService.show(
            this._translateService.instant('Departments.AddPlacesMessage.title'),
            this._translateService.instant(
              this.placesToBeInserted === 1
                ? 'Departments.AddPlacesMessage.one'
                : 'Departments.AddPlacesMessage.many',
              { value: this.placesToBeInserted }
            )
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true,
          });
        })
        .catch((err) => {
          this._errorService.showError(err, {
            continueLink: '.',
          });
        });
    }
    return this.close();
  }
}
