import {Component, Input, OnDestroy, OnInit,} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import {AppEventService, ModalService, ToastService} from '@universis/common';
import { ErrorService } from '@universis/common';

@Component({
  selector: 'app-group-courses-details-percent',
  templateUrl: './group-courses-details-percent.component.html',
})

export class GroupCoursesDetailsPercentComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy {


  @Input() groupCourses;
  public lastError: any;
  public validatorObject = {
    percentSum: 0,
    firstPercentValue: 0,
    identicalValues: true
  };

  constructor(protected _router: Router,
              protected _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _modalService: ModalService,
              private _appEventService: AppEventService) {
    // call super constructor.
    super(_router, _activatedRoute);
  }

  async ngOnInit() {
    // set up modal.
    this.modalTitle = this._translateService.instant('StudyPrograms.EditFactors');
    this.okButtonText = this._translateService.instant('StudyPrograms.Submit');
    this.cancelButtonText = this._translateService.instant('StudyPrograms.Cancel');
    this.modalClass = 'modal-xl';
    this.validatorObject.firstPercentValue = this.groupCourses[0].programGroupFactor
  }

  async ok() {
    // clear error.
    this.lastError = null;
    // validate percentages.
    const validPercentages = this.validatePercentages();
    // convert text to floats.
    this.groupCourses.forEach(course => {
      course.programGroupFactor = parseFloat(course.programGroupFactor);
    });
    if (validPercentages) {
      // gather and post.
      const toBeSaved = this.groupCourses.map(part => {
        return {
          id: part.id,
          programGroupFactor: part.programGroupFactor
        };
      });
      try {
        await this._context.model('StudyProgramCourses').save(toBeSaved);
        this._toastService.show(this._translateService.instant('Forms.EditFactorsShort'),
          this._translateService.instant('Forms.EditFactorsSuccess'));
        // fire event.
        this._appEventService.change.next({
          model: 'StudyProgramCourses'
        });
        // close.
        if (this._modalService.modalRef) {
          return this._modalService.modalRef.hide();
        }
      } catch (err){
        this.lastError = err;
        return false;
      }
    } else {
      // inform user.
      this.lastError = this._translateService.instant('Forms.InvalidPercentages');
      return false;
    }
  }

  ngOnDestroy() {
  }

  async cancel() {
    this._appEventService.change.next({
      model: 'StudyProgramCourses'
    });
    if (this._modalService.modalRef) {
      return this._modalService.modalRef.hide();
    }
  }

  checkIfValid(){
    console.log(this.groupCourses.map(x => parseFloat(x.programGroupFactor)))
    this.okButtonDisabled = !this.validatePercentages();
    if(this.okButtonDisabled){
      this.lastError = this._translateService.instant('Forms.InvalidPercentages');
    } else {
      if(this.lastError)
        this.lastError = null;
    }
  }

  validatePercentages(): boolean {
    this.validatorObject.percentSum = this.groupCourses.map(x => parseFloat(x.programGroupFactor)).reduce((a,b) => a+b, 0);
    this.validatorObject.identicalValues = this.groupCourses.map(x => parseFloat(x.programGroupFactor)).every((val, i, arr) => val === arr[0]);
    return (this.validatorObject.percentSum === 100 || this.validatorObject.percentSum === 1 || this.validatorObject.identicalValues);
  }
}
