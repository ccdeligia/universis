import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription, combineLatest} from 'rxjs';
import {ErrorService, ModalService, ToastService} from '@universis/common';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';


@Component({
  selector: 'app-instructors-dashboard-students',
  templateUrl: './instructors-dashboard-students.component.html'
})
export class InstructorsDashboardStudentsComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  public instructorID: any;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {

    this.dataSubscription = combineLatest(this._activatedRoute.params, this._activatedRoute.data)
      .subscribe((results) => {
          const params = results[0];
          this.instructorID = params.id;
          const data = results[1];
          this._activatedTable.activeTable = this.students;
          this.students.query = this._context.model('StudentCounselors')
            .where('instructor').equal(params.id)
            .expand('student($expand=person,department)')
            .orderByDescending('fromYear')
            .prepare();
          const loaded = this.tableConfiguration != null;
          this.students.config = this.tableConfiguration = data.tableConfiguration;
          if (loaded) {
            this.students.fetch(true);
          }
          this.search.form = this.searchConfiguration = data.searchConfiguration;
      });

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
