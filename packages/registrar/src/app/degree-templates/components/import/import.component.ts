import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppEventService, LoadingService } from '@universis/common';
import { DegreeTemplate, DegreeTemplateService } from '../../services/degree-template.service';

@Component({
  selector: 'app-degree-templates-import',
  templateUrl: './import.component.html',
  providers: [
    DegreeTemplateService
  ]
})
export class ImportComponent implements OnInit {

  public lastError: Error;
  public lastMessage: {message: string, type?: string};
  protected items: DegreeTemplate[];
  public files = [];

  constructor(private degreeTemplate: DegreeTemplateService,
    private loading: LoadingService,
    private translateService: TranslateService,
    private appEvent: AppEventService) { }

  ngOnInit() {
  }

  onSelect(event: {addedFiles: File[]}) {
    this.loading.showLoading();
    this.files = [];
    this.files.push(event.addedFiles[0]);
    this.degreeTemplate.parse(event.addedFiles[0]).then((items) => {
      this.items = items;
      if (items.length === 0) {
        this.lastMessage = {
          message: this.translateService.instant('DegreeTemplates.ImportTemplates.ΝοItemsFound'),
          type: 'alert-warning'
        };
      } else {
        this.lastMessage = {
          message: this.translateService.instant('DegreeTemplates.ImportTemplates.ItemsFound', {
            length: items.length
          }),
          type: 'alert-info'
        };
      }
      this.loading.hideLoading();
    }).catch((err) => {
      this.loading.hideLoading();
      this.lastError = err;
    });
  }
  onRemove() {
   this.lastError = null;
   this.lastMessage = null;
   this.files = [];
   this.items = [];
  }

  import() {
    this.lastError = null;
    this.loading.showLoading();
    this.degreeTemplate.import(this.items).then(() => {
      this.loading.hideLoading();
      this.appEvent.add.next({
        model: 'DegreeTemplates'
      });
    }).catch((err) => {
      this.lastError = null;
    });
  }

}
