import { GradeSubmissionsModule } from './grade-submissions.module';

describe('ExamsModule', () => {
  let examsModule: GradeSubmissionsModule;

  beforeEach(() => {
    examsModule = new GradeSubmissionsModule();
  });

  it('should create an instance', () => {
    expect(examsModule).toBeTruthy();
  });
});
